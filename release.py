import argparse
import datetime
import os
import re
import subprocess

from collections import defaultdict
from pathlib import Path
from pprint import pprint

import requests


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--pkg', type=str, required=True, help='Path to the package directory')
    parser.add_argument('--remote', type=str, default='https://gitlab.com/katalytic', help='The repo URL including the namespace, but excluding the package name')
    parser.add_argument('-y', action='store_true', help='Skip the prompt and assume the answer was "yes"')
    return parser.parse_args()


def main(pkg_path, remote):
    os.chdir(pkg_path)
    pkg_path = os.getcwd()
    new_changelog_section, new_version, version, versions = prepare_new_version_and_changelog_section(remote, pkg_path)
    if not should_release(new_changelog_section, new_version, version, versions):
        return

    update_toml_and_changelog(new_changelog_section, new_version, pkg_path)
    commit_tag_and_push(new_version)
    build(pkg_path)
    release(pkg_path)


def release(pkg_path):
    os.chdir(pkg_path)
    out, _ = shell('.venv/bin/python -m twine upload dist/*')
    print(out)


def build(pkg_path):
    prep_build_env()
    print('Building ...')
    shell('.venv/bin/python -m pdm build')
    shell('.venv/bin/python -m twine check dist/*')

    test_build(pkg_path, 'whl')
    test_build(pkg_path, 'tar.gz')
    print('Build OK')


def prep_build_env():
    shell('rm -rf *.egg-info dist/ build/')
    no_pdm = not list(Path('.venv/lib/').rglob('*/site-packages/pdm'))
    no_twine = not list(Path('.venv/lib/').rglob('*/site-packages/twine'))

    if no_pdm or no_twine:
        if not Path('.venv').exists():
            print('Creating venv ...')
            shell('python -m venv .venv')

        print('Installing pdm and twine ...')
        shell('.venv/bin/python -m pip install --upgrade pip')
        shell('.venv/bin/python -m pip install pdm twine')


def test_build(pkg_path, ext):
    if ext == 'whl':
        build_type = 'wheel'
    elif ext == 'tar.gz':
        build_type = 'sdist'
    else:
        raise ValueError(f'Unknown build type: {ext}')

    print(f'Testing {build_type} ...')
    pkg = pkg_path.split('/')[-1].replace('-', '.')

    os.chdir('/tmp')
    shell('rm -rf venv')
    shell(f'python3 -m venv .venv')
    if 'katalytic-images' in pkg_path:
        shell(f'.venv/bin/python -m pip install opencv-python')

    shell(f'.venv/bin/python -m pip install {pkg_path}/dist/*.{ext}')
    shell(f'.venv/bin/python -c "import {pkg}"')


def commit_tag_and_push(new_version):
    shell('git add pyproject.toml CHANGELOG.md')
    shell(f'git commit -m "v{new_version}"')
    shell(f'git tag -a v{new_version} -m "v{new_version}"')
    shell('git push --follow-tags')


def should_release(new_changelog_section, new_version, version, versions):
    print('Versions found:', versions)
    if version == new_version:
        print('No version bump')
        return False

    print(f'Version: {version} -> {new_version}')
    print(new_changelog_section)

    if ARGS.y:
        print('Skipped the prompt. Applying changes ...')
    else:
        x = input('Should I apply the changes and release? [y/N] ')
        if x.lower() not in ['y', 'yes']:
            print('Aborting ...')
            return False

    return True


def update_toml_and_changelog(new_changelog_section, new_version, pkg_path):
    toml_path = f'{pkg_path}/pyproject.toml'
    changelog_path = f'{pkg_path}/CHANGELOG.md'
    with open(toml_path, 'r') as f:
        toml = f.read()

    with open(changelog_path, 'r') as f:
        changelog = f.read()

    toml = re.sub(r'(version = "[0-9]+\.[0-9]+\.[0-9]+.*)"', f'version = "{new_version}"', toml)
    changelog = f'{new_changelog_section}\n{changelog}'

    with open(toml_path, 'w') as f:
        f.write(toml)

    with open(changelog_path, 'w') as f:
        f.write(changelog)


def prepare_new_version_and_changelog_section(remote, pkg_path):
    toml_path = f'{pkg_path}/pyproject.toml'
    commits = get_list_of_commits()
    commits = parse_commits(commits)
    pkg, _ = find_pkg_and_version(toml_path)

    versions = find_versions(toml_path, commits)
    version = pick_currently_highest_version(versions)
    commits = filter_commits_since_last_version(commits, versions['pypi'])

    new_version = calc_new_version(version, commits)
    new_changelog_section = generate_changelog_section(commits, new_version, remote, pkg)
    return new_changelog_section, new_version, version, versions


def generate_changelog_section(commits, new_version, remote, pkg):
    lines = [
        f'## {new_version} ({datetime.datetime.now().strftime("%Y-%m-%d")})',
    ]

    groups = defaultdict(list)
    for commit in commits:
        groups[commit['type']].append(commit)

    for g in ['feat', 'fix', 'perf', 'refactor', 'docs']:
        if g not in groups:
            continue

        lines.append(f'### {g}')
        for commit in sorted(groups[g], key=lambda c: c['message']):
            scope = f'**{commit["scope"]}:** ' if commit['scope'] else ''
            short_hash = commit['short_hash']
            long_hash = commit['hash']
            msg = commit["message"]

            lines.append(f'- [[`{short_hash}`]({remote}/{pkg}/commit/{long_hash})] {scope}{msg}')

    return '\n'.join(lines) + '\n\n'


def calc_new_version(version, commits):
    if not commits:
        return version
    elif version == '':
        return '0.1.0'

    major, minor, patch = map(int, version.split('.'))
    commit_types = {commit['type'] for commit in commits}
    if 'feat' in commit_types:
        minor += 1
        patch = 0
    elif commit_types.intersection(['fix', 'refactor', 'perf']):
        patch += 1

    return f'{major}.{minor}.{patch}'


def pick_currently_highest_version(versions):
    """pick the highest version JIC you've removed a broken version
    from pypi, because you can't reupload it
    """
    v = max(versions.values())
    if len(set(versions.values())) >= 2:
        print('Versions don\'t match: ', end='')
        pprint(versions)
        print(f'Will pick the highest version: {v}')

    return v


def filter_commits_since_last_version(log, pypi_version):
    """Use the pypi version because you want to include all commits
    after that version in the changelog. Even if you've released another
    version and deleted it from pypi after that
    """
    commits = []
    for commit in log:
        if pypi_version != '' and pypi_version == commit['version']:
            break

        commits.append(commit)

    return commits


def find_versions(toml_path, log):
    pkg, version = find_pkg_and_version(toml_path)
    return {
        'git': find_git_version(log),
        'pypi': find_pypi_version(pkg),
        'toml': version,
    }


def find_pkg_and_version(toml_path):
    with open(toml_path) as f:
        toml = f.read()

    pkg = re.search(r'name = "([a-z0-9\-]+)"', toml)
    pkg = pkg.group(1) if pkg else ''

    version = re.search(r'version = "([0-9]+\.[0-9]+\.[0-9]+.*)"', toml)
    version = version.group(1) if pkg else ''

    return pkg, version


def find_pypi_version(package):
    r = requests.get(f'https://pypi.org/rss/project/{package}/releases.xml')
    if r.status_code == 200:
        pattern = r'<link>https://pypi.org/project/' + package + r'/([0-9]+\.[0-9]+\.[0-9]+.*)\/</link>'
        versions = re.findall(pattern, r.text)
        if versions:
            # convert to tuple of ints, pick max, convert back to str
            # because python sorts one char at a time, so v0.9.0 > v0.10.0
            versions = [tuple(map(int, v.split('.'))) for v in versions]
            return '.'.join(map(str, max(versions)))
        else:
            return ''
    else:
        return ''


def find_git_version(log):
    for line in log:
        if line['version']:
            return line['version']

    return ''


def get_list_of_commits():
    git_log = subprocess.check_output(['git', 'log', '--pretty=format:"%h ### %H ### %D ### %s ### %b"'])
    return git_log.decode('utf-8').splitlines()


def parse_commits(log):
    parsed_log = []
    for line in log:
        if line == '"':
            continue

        line = line.strip('"')
        line = line.split(' ### ')

        commit_scope, commit_type, msg = extract_commit_info(line)
        parsed_log.append({
            'short_hash': line[0],
            'hash': line[1],
            'version': extract_version_from_tags(line[2]),
            'subject': line[3],
            'body': line[4],
            'scope': commit_scope,
            'type': commit_type,
            'message': msg,
        })

    return parsed_log


def extract_version_from_tags(tags):
    """The pattern r'tag: v([0-9]+.[0-9]+.[0-9]+[^,]*),?' matches
    this format "tag: v1.2.3[.extra]" and converts it to "1.2.3[.extra]"
    """
    match = re.search(r'tag: v([0-9]+\.[0-9]+\.[0-9]+[^,]*),?', tags)
    if match:
        return match.group(1)
    else:
        return ''


def extract_commit_info(line):
    pattern = r'^(misc|dev|docs|feat|fix|perf|refactor|style|test)(?:\(([^)]+)\))?: ?(.*)'
    match = re.search(pattern, line[3])
    if match:
        commit_type, commit_scope, msg = match.groups()
        if commit_scope is None:
            commit_scope = ''
    else:
        commit_type = commit_scope = msg = ''

    return commit_scope, commit_type, msg


def shell(cmd, check=True):
    r = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding='utf-8')
    output = r.stdout.strip()
    if check and r.returncode != 0:
        raise Exception(f'Command `{cmd}` failed with exit code {r.returncode}:\n{output}')

    return (output, r.returncode)


if __name__ == '__main__':
    ARGS = parse_args()
    main(ARGS.pkg, ARGS.remote)
